<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Implements new user registering functionality.
 *
 */

require_once 'autoloader.php';

class RegisterUserRequest extends Request
{
    /**
     * @return boolean
     */
    public function onPost()
    {
        $user = $this->getParams()[0];
        $email = $this->getParams()[1];
        $password = $this->getParams()[2];
        $recaptchaResponse = $this->getParams()[3];

        if (!$this->valInput($user, $email, $password,$recaptchaResponse )) {
            return false;
        } else {
            $db = Database::getInstance();
            $res = $db->addUser($user, password_hash($password, PASSWORD_BCRYPT), password_hash($email, PASSWORD_BCRYPT));

            return $res;
        }
    }

    public function valInput($user, $email, $password,$recaptchaResponse )
    {

        if (strlen($user) > 50 || strlen($password) <= 8 || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        if (!preg_match("#.*^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\W]).*$#", $password)) {
            return false;
        }

        if (!$this->validateCaptcha($recaptchaResponse)) {
            return false;
        }
        return true;
    }

    public function validateCaptcha($recaptchaResponse)
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            // Doesn't work, returns null.
             //if(isset($_POST['recaptchaResponse']) && !empty($_POST['recaptchaResponse'])){
            $secret = '6LdbU10UAAAAAED3zgUjIgbsG4TqBnEur-fev21V';
            // $response = $_POST['recaptchaResponse'];
            $response = $recaptchaResponse;
            $ip = $_SERVER['REMOTE_ADDR'];

            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $response . "&remoteip=" . $ip);
            $responseData = json_decode($verifyResponse);
            if ($responseData->success) {
                return true;
            } else {
                alert('Invalid reCAPTCHA');
                return false;
            }
            }
        }
   // }
}
