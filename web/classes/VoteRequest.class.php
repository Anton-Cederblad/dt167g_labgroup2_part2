<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Implements up voting or down voting of a message.
 */

require_once 'autoloader.php';

class VoteRequest extends Request {
    /**
     * Sends vote data to database. If vote == null, call Database::removeVote instead
     * @return mixed
     */
    public function onPost() {

        $user = $this->getUser();

        // No user logged in
        if ($user === "") {
            return false;
        }

        $msgId = $this->getParams()[0];
        $vote = $this->getParams()[1];

        $db = Database::getInstance();

        if($vote == null) {
            return $db->removeVote($user, $msgId);
        } else {
            return $db->vote($user, $msgId, $vote);
        }
    }
}