<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Implements post message requirement.
 */

require_once 'autoloader.php';

class PostMessageRequest extends Request {
    /**
     * @return boolean
     */
    public function onPost() {
        $content = $this->getParams();

        // Don't post something that isn't a string or empty
        if (gettype($content) !== "string" || empty($content) || strlen($content) > 250) {
            return false;
        }
        $user = $this->getUser();

        $db = Database::getInstance();
        return $db->postMessage($user, $content);
    }
}
