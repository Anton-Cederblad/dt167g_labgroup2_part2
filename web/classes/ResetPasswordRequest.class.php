<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Implements password reset functionality.
 *
 */

require_once 'autoloader.php';

class ResetPasswordRequest extends Request {
    /**
     * @return boolean
     */
    public function onPost() {
        $content = $this->getParams();
        $db = Database::getInstance();

        if($db -> countLog("BAD TOKEN INPUT", $_SERVER['REMOTE_ADDR']) < 5){ // You get 5 tries every 30min to random guess tokens

            // Hash the provided inputs.
            $token = hash("sha256", $content[0]);
            $pwd = password_hash($content[1], PASSWORD_BCRYPT);
      

            // Reset the users password.
            $res = $db -> resetPwdHash($token, $pwd);
            if(!$res){
                $db-> addLog("BAD TOKEN INPUT",$_SERVER['REMOTE_ADDR']);
            }else{
                // Remove/invalidate the resetPwdToken
                $db -> removeResetPwdToken($token);
            }

            return $res;
        }else{
            return false;
        }
    }

}