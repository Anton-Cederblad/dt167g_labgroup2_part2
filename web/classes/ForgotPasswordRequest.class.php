<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Implements forgot password functionality.
 * Requires the program "sendmail" on the (linux)server and its service up and running.
 * Tip: I needed to add an empty file named "local-host-names" in the config-folder for sendmail to get the service
 *      running. (On my computer(arch linux), the folder to add it in was /etc/mail)
 */

require_once 'autoloader.php';

class ForgotPasswordRequest extends Request {

    // FOR TESTING PURPOSES ONLY!
    // If true, the password reset mail will be sent to the supplied mail address regardless of if it matches the
    // users mail or not.
    private $allowAnyMailAddress = false;

    /**
     * @return null
     */
    public function onPost() {
        $content = $this->getParams();

        $username = $content[0];
        $mailInput = $content[1];


        $db = Database::getInstance();
        $tmpHash = password_hash("This could not possibly be a valid email address", PASSWORD_BCRYPT);
        $res = $db -> getMailHash($username);

        if ($res)
            $mailHash = $res;
        else
            $mailHash = $tmpHash;


        if ( password_verify(strtolower($mailInput), $mailHash) || $this -> allowAnyMailAddress ) {
            $this -> sendResetMail($username, $mailInput);
        } else {
            // Perform most of the work in sendResetMail, even if the mail is invalid.
            $this -> sendResetMail(null, null);
        }

        return null;
    }

    private function sendResetMail($username, $mail) {
        $db = Database::getInstance();

        if($db -> countLog("PASSWORD TOKEN CREATED", $username) < 1){ // Only allow this to be done once every 30min
        // Generate random token
            try {
                $token = bin2hex(random_bytes(30));
            } catch (Exception $e) {
                return;
            }

            // Sanity check, just in case random_bytes breaks or something crazy like that.
            if (strlen($token) != 60)
                return;

            $fromAddress = "noreply@group1security.dt167g.miun.se";
            $subject = "Forgotten password reset";

            $baseDomain = "http://" . $_SERVER['HTTP_HOST'];
            $resetPasswordPage = str_replace($_SERVER['DOCUMENT_ROOT'], $baseDomain, realpath("resetpassword.php"));
            

            $mailBody  = "Hi " . $username . ",\r\n\r\n";
            $mailBody .= "Somebody requested a new password for your guestbook account on messageapp. No changes ";
            $mailBody .= "have been made on your account yet.\r\n\r\n";
            $mailBody .= "You can reset your password by clicking on the link below: \r\n\r\n";
            $mailBody .= $resetPasswordPage . "?email=" . $mail . "&token=" . $token;


            if ($mail !== null && $username !== null) {

                // Update database
                
                $res = $db -> setResetPwdToken($username, hash("sha256", $token));

                // If everything went OK, send the mail.
                if ($res)
                    mail($mail, $subject, $mailBody, "From: " . $fromAddress);
            }
        }
        return;

    }
}