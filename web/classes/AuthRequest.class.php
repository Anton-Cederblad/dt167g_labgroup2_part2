<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Handles things related to user authentication.
 *
 * Returns currently logged in user
 * Accepts:
 *  { "command":"getuser" }
 * Returns:
 *  When a user is logged in: "<username>"
 *  When no user is logged in: ""
 *
 * Log in a user with specified credentials
 * Accepts:
 *  { "command":"login", "username":"<user name>", "password":"<password>" }
 * Returns:
 *  Login was successful: "<user name of logged in user>"
 *  On failure, for any reason: ""
 *
 * Logout a user
 * Accepts:
 *  { "command":"logout" }
 * Returns: "ok"
 *
 */

require_once 'autoloader.php';

class AuthRequest extends Request {
    /**
     * @return string See the explanation above.
     */
    public function onPost() {
        $msg = $this->getParams();

        if (!isset($msg['command'])) {
            return null;
        }

        switch($msg['command']) {
            case "getuser":
                return $this->onGetUser();
            case "login":
                return $this->onLogin($msg);
            case "logout":
                return $this->onLogout();
            default:
                return null;
        }
    }

    /**
     * @return string User name of the currently logged in user. "" if no user is
     * logged in.
     */
    private function onGetUser() {
        return $this->getUser();
    }

    /**
     * Tries to login a user with credentials specified in $msg.
     * @param $msg array parameters.
     * @return string User name of newly logged in user. "" on failure.
     */
    private function onLogin($msg) {
        if (!isset($msg['username']) || !isset($msg['password']) ) {
            return "";
        }

        $user = $msg['username'];
        $pass = $msg['password'];

        // Removing bad old log that used default error_log, also both if login was good or bad it didnt say.
        //error_log("User:" . $user . " Pass:" . $pass);

        if (empty($pass) || empty($user)) {
            return "";
        }

        // Get password hash for specified user
        $db = Database::getInstance();

        // Check if account is locked out, lock at 5
        if($db -> countLog("BAD LOGIN", $user) < 5){ 

            $hash = $db->getPasswordHash($user);
            if ($hash == false) {
                return "";
            }

            if (password_verify($pass, $hash)) {
                session_start();
                $_SESSION['user'] = $user;
                session_regenerate_id(); // Update session ID after user logs in
                $_SESSION['CREATED'] = time(); // Set session time
                session_write_close();
                $db->addLog("GOOD LOGIN",$user);
                return $user;
            } else {
                $db->addLog("BAD LOGIN",$user);
                return "";
            }

        }else{
            // Account locked out
            return "Account is locked out. Please wait a few min before trying again.";
        }
    }

    /**
     * Logout current user.
     * @return string Always "ok".
     */
    private function onLogout() {
        // Clear current user
        session_start();
        unset($_SESSION['user']);
        session_write_close();
        return "ok";
    }
}