<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Handles all AJAX requests.
 *
 * AJAX requests is sent as JSON with the following syntax:
 * {
 *     request:"request name"
 *     params:<any value> (optional)
 * }
 *
 * Explanation:
 * request: request name, one of keys in RequestHandler::$classes
 * params: some json value that is sent to the Request object.
 *
 * An example of how this might work:
 * 1. Client/browser sends '{ "request":"vote", "params":{"msgid":"1234", "vote":"up" } }';
 * 2. RequestHandler receives the message and creates a VoteRequest instance.
 * 3. Instance VoteRequest up votes message with id 1234 and returns true from processRequest().
 * 4. getResponse() provides some kind of response to let the client know the up vote went ok.
 * 5. RequestHandler sends response to client.
 *
 */

require_once 'autoloader.php';

class RequestHandler {
    /**
     * Request names and which classes should be used to handle those. Add
     * new requests here.
     * @var array
     */
    private static $classes = [
        'postmsg'  => 'PostMessageRequest',
        'delmsg'   => 'DeleteMessageRequest',
        'getmsg'   => 'GetMessagesRequest',
        'vote'     => 'VoteRequest',
        'forgotpwd'=> 'ForgotPasswordRequest',
        'resetpwd' => 'ResetPasswordRequest',
        'auth'     => 'AuthRequest',
        'reguser'  => 'RegisterUserRequest'
    ];

    /**
     * POST data sent in HTTP header.
     * @var array
     */
    private $data;

    /**
     * Request instance that handles current request.
     * @var Request
     */
    private $request;

    public function __construct() {
        $this->data = $this->getData();
    }

    /**
     * Reads request from HTTP header(POST) and handles specified request.
     */
    public function handleRequest() {
        // A request name has to be specified
        if (!isset($this->data['request'])) {
            $this->sendErrorResponse();
            return;
        }

        // Create a Request object that will handle this request
        $this->request = $this->createRequestInstance();
        if ($this->request === null) {
            $this->sendErrorResponse();
            return;
        }

        // Handle any parameters for the request
        if (isset($this->data['params'])) {
            // Filter request params to prevent XSS
            $this->filterParams();

            $this->request->setParams($this->data['params']);
        }

        $this->request->setUser($this->getCurrentUser());

        // Let the Request object decide what to send to the client.
        $response = $this->request->onPost();

        $this->sendResponse($response);
    }

    /**
     * Tell the client that the request couldn't be processed for some reason.
     */
    private function sendErrorResponse() {
        // I don't know what the appropriate status code would be here. But as
        // long as it isn't 200 the client will know the request wasn't
        // successful.
        header("HTTP/1.0 400 Bad Request");
    }

    /**
     * Sends a response to client. $response is sent as a JSON string.
     * @param $response mixed Response value.
     */
    private function sendResponse($response) {
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /**
     * @return array Request data as an array. Empty array of nothing was received.
     */
    private function getData() {
        $data = file_get_contents('php://input');

        if ($data == false) {
            return [];
        }

        return json_decode($data, true);
    }

    private function getClassName($requestName) {
        if (isset(self::$classes[$requestName])) {
            return self::$classes[$requestName];
        } else {
            return null;
        }
    }

    /**
     * Creates a Request instance that should handle the current request.
     */
    private function createRequestInstance() {
        $className = $this->getClassName($this->data['request']);

        if ($className !== null) {
            return new $className($this->data['params']);
        } else {
            return null;
        }
    }

    /**
     * Filter request params to prevent XSS.
     */
    private function filterParams() {
        // todo: Should $this->data['params'] be filtered here or by each Request object?
        // It feels like a good idea to have this centralized.
    }

    /**
     * @return string Username of current user.
     */
    private function getCurrentUser() {
        // Get user name from current session if there is one.
        $currentUser = "";
        session_start();
        if (isset($_SESSION['user'])) {
            if (!isset($_SESSION['CREATED'])) {
                $_SESSION['CREATED'] = time();
                $currentUser = $_SESSION['user'];
            } else if (time() - $_SESSION['CREATED'] < 1800) {       
                session_regenerate_id(); // Update session ID
                $_SESSION['CREATED'] = time();  // update creation time
                $currentUser = $_SESSION['user'];
            }else{
                // session started more than 30 minutes ago
                // Time out, kill session and return empty user
                session_unset();
                session_destroy();
            } 
        }
        session_write_close();
        return $currentUser;
    }
}
