<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Base class for request handlers.
 */


class Request {
    /**
     * Request parameters.
     * @var array
     */
    private $params = null;

    /**
     * User for this request.
     * @var string
     */
    private $username = "";

    /**
     * Called when a POST request is received from the client. Request parameters
     * is accessed with getParams(). The function should return what should be
     * sent to the user, this could be anything as long as it can be converted
     * to JSON.
     * @return mixed Response that is sent to the client.
     */
    public function onPost() {
        return null;
    }

    /**
     * @param $params array Set request parameters.
     */
    public function setParams($params) {
        $this->params = $params;
    }

    /**
     * @return array Current request parameters.
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * Which user should this request use.
     * @param $username string User name
     */
    public function setUser($username) {
        $this->username = $username;
    }

    /**
     * @return string Which user this request applies to.
     */
    public function getUser() {
        return $this->username;
    }
}