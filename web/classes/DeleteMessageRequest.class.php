<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Implementation of delete message requirement.
 */

require_once 'autoloader.php';

class DeleteMessageRequest extends Request {
    /**
     * @return mixed
     */
    public function onPost() {
        $messageId = $this->getParams();

        $db = Database::getInstance();

        $owner = $db->getMessageOwner($messageId);

        if ($owner === $this->getUser()) {
            return $db->deleteMessage($messageId);
        } else {
            return false;
        }
    }
}