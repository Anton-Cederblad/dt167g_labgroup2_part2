<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Fetches all messages in the database and sends those to the client.
 *
 */

require_once 'autoloader.php';

class GetMessagesRequest extends Request {
    /**
     * @return array Same format as Database->getMessages() returns.
     */
    public function onPost() {
        $db = Database::getInstance();
        return $db->getMessages();
    }
}