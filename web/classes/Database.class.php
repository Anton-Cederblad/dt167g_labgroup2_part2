<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Implementation of Database class, for handling database communication.
 */

class Database {
    private $db;
    private static $instance;

    private function __construct() {
        // This section needs to be changed to match the setup in the
        // virtual machine.
        $dbType = "pgsql";
        $dbHost = "localhost";
        $dbPort = 5432;
        $dbName = "group1security";
        $dbUsername = "postgres";
        $dbPwd = "postgres";


        $dbDSN = $dbType . ":host=" . $dbHost . ";port=" . $dbPort . ";dbname=" . $dbName;

        $this -> db = new PDO($dbDSN, $dbUsername, $dbPwd);

    }

    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Database();
        }

        return self::$instance;
    }

    // Helper function for getting the userId, regardless of if a string (username) or int (userId) is passed.
    // Returns: the userId (int) or false if the user is not found.
    private function getUserId($user) {
        if ( is_int($user) ) // $user already is an userId
            return $user;
        elseif ( is_string($user) ) { // $user is an username

            // Fetch the userId corresponding to the username.

            $query = "SELECT id FROM messageapp.user WHERE username = :username";

            $statement = $this -> db -> prepare($query);

            $statement -> bindValue('username', $user);

            $statement -> execute();

            if ($statement)
                $res = $statement -> fetch();
            else
                $res = false;

            if ( $res )
                return $res['id'];
            else
                return false;

        } else {
            return false;
        }
    }

    // Name: postMessage
    // Desc: Register a new post in the database
    // Params: $user - the userId(int) or username(string) of the user who posted the message
    //         $content - (string) the content of the message
    // Returns: true if everything went OK, false otherwise (user doesn't exist?).
    public function postMessage($user, $content) {

        $userId = $this -> getUserId($user);

        if (!$userId)
            return false;


        if($this -> countLog("MESSAGE CREATED", $userId) < 100){ // stop to many post by limit max messages every 30min to 100
            $query = "INSERT INTO messageapp.post(userId, content) VALUES(:id, :content)";

            $statement = $this -> db -> prepare($query);

            $statement -> bindValue('id', $userId);
            $statement -> bindValue('content', $content);

            $result = $statement -> execute();
            if($result){
                $this -> addLog("MESSAGE CREATED",$userId);
            }
            return $result;
        }else{
            return false;
        }
        
    }


    // Name: deleteMessage
    // Desc: Deletes a post.
    // Params: $msgId(int) - the id of the message to be deleted.
    // Returns: true if everything went OK, false otherwise.
    public function deleteMessage($msgId) {

        $query = "DELETE FROM messageapp.vote WHERE postid = :id";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('id', $msgId);

        $statement -> execute();

        $query = "DELETE FROM messageapp.post WHERE id = :id";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('id', $msgId);

        $statement -> execute();

        // returns false if no rows were affected by the DELETE command.
        $result = ( $statement -> rowCount() != 0 );
        if($result){
            // Should save the user deleting it here aswell
            $this -> addLog("MESSAGE DELETED","POST:".$msgId);
        }
        return $result;
    }


    // Name: getMessageOwner
    // Desc: Returns the username of the owner/creator of the message
    // Params: $msgId - (int) the id of the message to get the owner to..
    // Returns: (string)username of the owner, or false if anything went wrong (message probably doesn't exist)
    public function getMessageOwner($msgId) {
        $query = "
              SELECT username
              FROM messageapp.post LEFT JOIN messageapp.user ON userId = messageapp.user.id
              WHERE messageapp.post.id = :post";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('post', $msgId);

        $statement -> execute();

        if (!$statement)
            return false;

        $res = $statement -> fetch(PDO::FETCH_ASSOC);

        if ( isset($res['username']) )
            return $res['username'];
        else
            return false;
    }


    // Name: getMessages
    // Desc: Get all available messages, including "vote-score"
    // Returns: An array of messages(arrays) with the fields:
    //              id - (int) the message ID
    //              time - (string) timestamp in the format yyyy-mm-dd hh:mm
    //              content - (string) the content of the message.
    //              username - (string)
    //              vote - (int) the number of upvotes minus the number of downvotes
    public function getMessages() {
        $query =
            "SELECT messageapp.post.id AS id, username, time, content, COALESCE(vote, 0) AS vote
             FROM (messageapp.post LEFT JOIN
             (SELECT postId, SUM(vote) AS vote FROM messageapp.vote GROUP BY postId) AS qry1
             ON id = postId) LEFT JOIN
             messageapp.user ON messageapp.user.id = userId";

        $statement = $this -> db -> query($query);

        $res = [];
        while ($msg = $statement -> fetch(PDO::FETCH_ASSOC)) {
            $msg['time'] = date("Y-m-d H:i", strtotime($msg['time']));
            $res[] = $msg;
        }

        return $res;

    }


    // Name: vote
    // Desc: Registers or updates a users vote on a message.
    // Params: $user - userId(int) or username(string) of the voting user
    //         $msgId - (int) the id of the message the user is voting on
    //         $vote - (string) "up" or "down" depending on if the user upvotes or downvotes the message.
    // Returns: true if all went OK, false otherwise (user or msg doesn't exist?).
    public function vote($user, $msgId, $vote) {

        $userId = $this -> getUserId($user);

        if (!$userId)
            return false;

        $msgOwner = $this -> getUserId($this -> getMessageOwner($msgId));
        if ($userId === $msgOwner)
            return false;

        if ( $vote === "up")
            $voteNum = 1;
        elseif ( $vote === "down")
            $voteNum = -1;
        else
            return false;


        $query = "
          INSERT INTO messageapp.vote(userId, postId, vote)
          VALUES(:user, :post, :vote)
          ON CONFLICT(userId, postId)
          DO UPDATE SET vote = EXCLUDED.vote";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('user', $userId);
        $statement -> bindValue('post', $msgId);
        $statement -> bindValue('vote', $voteNum);

        $result = $statement -> execute();
        if($result){
            $this -> addLog("VOTE CREATED","USER:".$userId.",POST:".$msgId);
        }
        return $result;
    }


    // Name: removeVote
    // Desc: Removes an already registered vote
    // Params: $user - userId(int) or username(string) of the user wishing to remove the vote
    //         $msgId - (int) the messsage id to remove the vote from.
    // Returns: true if everything went OK, false otherwise (user or msg doesn't exist?).
    public function removeVote($user, $msgId) {

        $userId = $this -> getUserId($user);

        if (!$userId)
            return false;

        $query = "DELETE FROM messageapp.vote WHERE userId = :user AND postId = :post";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('user', $userId);
        $statement -> bindValue('post', $msgId);

        $statement -> execute();

        // returns false if no rows were affected by the DELETE command
        $result = ($statement -> rowCount() != 0);
        if($result){
            $this -> addLog("VOTE DELETED","USER:".$userId.",POST:".$msgId);
        }
        return $result;
    }


    // Name: addUser
    // Desc: Registers a new user
    // Comment: Perhaps the hashing could/should be done in this function? And gets the plaintext password/mail instead?
    // Params: $username - (string)
    //         $pwdHash - (string) the hash of the new users password
    //         $mailHash - (string) the hash of the new users mail-address or null if no address is given.
    // Returns: true if everything went OK, false otherwise(The username probably already exists)
    public function addUser($username, $pwdHash, $mailHash) {

        $query = "INSERT INTO messageapp.user(username, pwdHash, mailHash) VALUES(:user, :pwd, :mail)";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('user', $username);
        $statement -> bindValue('pwd', $pwdHash);
        $statement -> bindValue('mail', $mailHash);

        

        $result = $statement -> execute();
        if($result){
            $this -> addLog("USER CREATED",$username);
        }
        return $result;
    }


    // Name: setPasswordHash
    // Desc: Sets a new password(hash) for a user.
    // Comment: Perhaps the hashing could/should be done in this function? And gets the plaintext password instead?
    // Params: $user - userId(int) or username(string)
    //         $newPwdHash - the new password hash
    // Returns: true if everything went OK, false otherwise.
    public function setPasswordHash($user, $newPwdHash) {

        $userId = $this -> getUserId($user);

        if (!$user)
            return false;

        $query = "UPDATE messageapp.user SET pwdHash = :newPwd WHERE id = :user";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('user', $userId);
        $statement -> bindValue('newPwd', $newPwdHash);

        return $statement -> execute();
    }

    // Name: getPasswordHash
    // Desc: Gets the password(hash) for a user
    // Comment: If the password hashing happens in this class (see previous comments) then this function should be
    //          replaced by a checkPassword function that returns a boolean depending on if the password is correct or not.
    //          i.e. either all hashing happens in this class, or none, to keep that logic in one place.
    // Params: $user - userId(int) or username(string)
    // Returns: (string)password_hash or false if error (user doesn't exist?)
    public function getPasswordHash($user) {

        $userId = $this -> getUserId($user);

        if (!$userId)
            return false;

        $query = "SELECT pwdHash FROM messageapp.user WHERE id = :id";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('id', $userId);

        $statement -> execute();

        if (!$statement)
            return false;

        $res = $statement -> fetch(PDO::FETCH_ASSOC);

        if ( isset($res['pwdhash']) )
            return $res['pwdhash'];
        else
            return false;

    }


    // Name: setMailHash
    // Desc: sets a new mail hash for a user
    // Comment: Perhaps the hashing could/should be done in this function? And gets the plaintext mail instead?
    // Params: $user - userId(int) or username(string)
    //         $newMailHash - (string)
    // Returns: true if everything went OK, false otherwise.
    public function setMailHash($user, $newMailHash) {

        $userId = $this -> getUserId($user);

        if (!$userId)
            return false;

        $query = "UPDATE messageapp.user SET mailHash = :newMail WHERE id = :user";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('user', $userId);
        $statement -> bindValue('newMail', $newMailHash);

        return $statement -> execute();
    }


    // Name: getMailHash
    // Desc: Gets the hash of the users email address.
    // Params: $user - userId(int) or username(string)
    // Returns: the hash (string) if everything went OK, false otherwise
    public function getMailHash($user) {

        $userId = $this -> getUserId($user);

        if (!$userId)
            return false;

        $query = "SELECT mailHash FROM messageapp.user WHERE id = :user";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('user', $userId);

        $statement -> execute();

        if (!$statement)
            return false;

        $res = $statement -> fetch(PDO::FETCH_ASSOC);

        if (empty($res))
            return false;

        return $res['mailhash'];
    }

    // Name: setResetPwdToken
    // Desc: sets the resetPwdToken for the user
    // Params: $user - userId(int) or username(string)
    //         $token - (string) the hashed resetPwdToken
    // Returns: true if everything went OK, false otherwise
    public function setResetPwdToken($user, $token) {

        $userId = $this -> getUserId($user);

        if (!$userId)
            return false;

        $query = "UPDATE messageapp.user SET resetPwdToken = :token, resetPwdTime = " . time() . " WHERE id=:user";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('user', $userId);
        $statement -> bindValue('token', $token);
        $statement -> execute();

        $result = ($statement -> rowCount() != 0);
        if($result){
            $this -> addLog("PASSWORD TOKEN CREATED",$user);
        }
        return $result;
    }

    // Name: getMailHashFromToken
    // Desc: gets the mailHash for the user whose resetPwdToken matches the supplied one.
    // Params: $mailHash - (string)
    // Returns: resetPwdToken(string) IF it exists and IF resetPwdTime is within 24 hours from now, false otherwise.
    public function getMailHashFromToken($token) {


        $query = "SELECT mailHash, resetPwdTime FROM messageapp.user WHERE resetPwdToken=:token";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('token', $token);

        $statement -> execute();

        if (!$statement)
            return false;

        $res = $statement -> fetch(PDO::FETCH_ASSOC);

        if (!$res)
            return false;

        if (!isset($res['resetpwdtime']) || $res['resetpwdtime'] === null || time() - $res['resetpwdtime'] > 24*60*60)
            return false;

        return $res['mailhash'];


    }

    // Name: removeResetPwdToken
    // Desc: removes the resetPwdToken from the database
    // Params: $token(string) the hashed token to be removed from the database
    public function removeResetPwdToken($token) {

        $query = "UPDATE messageapp.user SET resetPwdToken=NULL, resetPwdTime=NULL WHERE resetPwdToken=:token";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('token', $token);

        $statement -> execute();
    }


    // Name: resetPwdHash
    // Desc: Updates the password hash for the user which corresponds to the supplied resetPwdToken
    // Params: $token - (string) the resetPwdToken
    //         $newPwd - (string) the hash of the new password
    // Returns: true if everything went OK, false otherwise
    public function resetPwdHash($token, $newPwd) {

        $query = "UPDATE messageapp.user SET pwdHash=:pwd WHERE resetPwdToken=:token";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('pwd', $newPwd);
        $statement -> bindValue('token', $token);

        $statement -> execute();

        $result = ($statement -> rowCount() != 0);
        if($result){
            // TODO
            // SQL to get user id with that token
            // User that for the log
            $this -> addLog("PASSWORD RESET","USER: IDNR");
        }
        return $result;
    }

    // Name: addLog
    // Desc: Add event to log
    // Comment: Not restrictions to amount added to this, should be checked before it happens
    // Params: $event - (string) what happend
    //         $value- (string) how did it happen
    // Returns: void
    public function addLog($event, $value) {

        $query = "INSERT INTO messageapp.log(event, value) VALUES(:eve, :val)";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('eve', $event);
        $statement -> bindValue('val', $value);

        $statement -> execute();
    }

    // Name: countLog
    // Desc: count logs 
    // Comment: Use to see how many are logged, used for lockouts. Counts the ones made 30min or closer only!
    // Params: $event - (string)
    //         $value- (string)
    // Returns: returns false if something is wrong, else int of found rows
    public function countLog($event, $value) {

        $query = "SELECT COUNT(*) FROM messageapp.log WHERE event =:eve AND value =:val AND stamp > now() - INTERVAL '30 minutes'";

        $statement = $this -> db -> prepare($query);

        $statement -> bindValue('eve', $event);
        $statement -> bindValue('val', $value);

        $result = $statement -> execute();
        if($result === false){
            return false;
        }else{
            return ($statement -> fetch(PDO::FETCH_ASSOC))['count'];
        }
    }

}
