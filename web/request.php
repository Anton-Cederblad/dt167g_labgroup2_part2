<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Securiry
 *
 * Implemented in RequestHandler class.
 */

require 'autoloader.php';

$rh = new RequestHandler();
$rh->handleRequest();