<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Main page
 */

?>

<!DOCTYPE html>
<html lang="sv-SE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/ajax.js"></script>
    <script src="js/auth.js"></script>
    <script src="js/filters.js"></script>
    <script src="js/messages.js"></script>
    <script src="js/sort.js"></script>
    <title>Guestbook</title>
</head>
<body>
<header>
    <h1>Software Security Lab</h1>
</header>
<main>
    <aside>
        <div id="login" class="hidden">
            <h2>LOGIN</h2>
            <form id="loginForm">
                <label><b>Username</b></label>
                <input type="text" placeholder="Enter username" name="uname" id="uname"
                       required maxlength="10" >
                <label><b>Password</b></label>
                <input type="password" autocomplete="off" placeholder="Enter Password" name="psw" id="psw"
                       required>
                <button type="button" id="loginButton">Login</button>
            </form>
            <a href="forgotpassword.php">Forgot password?</a>
            <br>
            <a href="registeruser.php">New user?</a>
        </div>
        <div id="logout" class="hidden">
            <h2>LOGOUT</h2>
            <p>Logged in as<span id="loggedInUser"></span></p>
            <form id="logoutForm">
                <button type="button" id="logoutButton">Logout</button>
            </form>
        </div>
        <p id="authMsg">Waiting for login status...</p>
    </aside>
    <section>
        <h2>Guestbook</h2>
        <div id="postMessage" class="hidden">
            <form action="" method="POST">
                <fieldset>
                    <legend>Post a new message</legend>
                    <textarea id="text" name="text"
                              rows="10" cols="50" aria-label="Message text"></textarea>
                    <br>
                    <button type="button" id="submitMessage" disabled>Post</button>
                    <p id="messagerr"></p>
                </fieldset>
            </form>
        </div>
        <br>
        <input type="text" id="userFilter" onkeyup="searchFilter('userFilter')" placeholder="Find messages from user.." title="user">
        <input type="text" id="keywordFilter" onkeyup="searchFilter('keywordFilter')" placeholder="Find messages containing..." title="keyword">
        <table id="guestbookEntries">
            <tr>
                <!--onClicks that corresponds with sort.js -->
                <th class="th20 sortHover" onClick="sortTable(0)" id="from">FROM
                </th>
                <th class="th40 sortHover" onClick="sortTable(1)" id="message">MESSAGE
                </th>
                <th class="th20 sortHover" onClick="sortTable(2)" id="time">TIME
                </th>
                <th class="th20 sortHover" onClick="sortTable(3)" id="votes">VOTES
                </th>
                <th class="th20">Delete message
                </th>
            </tr>
            <!-- Messages will be added here by messages.js -->
        </table>
    </section>
</main>
<footer>
</footer>
</body>
</html>
