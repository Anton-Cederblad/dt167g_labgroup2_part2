/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Handles clicks on the button on the forgotpassword page.
 *
 */

function doResetPassword() {

    let uname = document.getElementById("uname").value;
    let mail = document.getElementById("email").value;

    let form = document.getElementById("forgotPwdForm");

    // Hide form
    if (!form.classList.contains("hidden"))
        form.classList.add("hidden");

    let mailSentText = document.getElementById("mailSent");

    // Unhide text that informs the user that the mail is sent.
    if (mailSentText.classList.contains("hidden"))
        mailSentText.classList.remove("hidden");

    ajax.post("forgotpwd", [uname, mail], null);
}

function init() {
    let button = document.getElementById("forgotPwdButton");
    button.addEventListener('click', doResetPassword);
}

window.addEventListener('load', init, false);