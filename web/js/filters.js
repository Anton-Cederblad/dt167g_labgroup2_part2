/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Seach filters for lab
 *
 */

/**
 * Keeps track of last filter type used.
 * @type {string}
 */
var lastFilterUsed = "";

/**
 * Search filter. Shows only rows with text matching the input string.
 * @param filterType String describing what to filter
 */

function searchFilter(filterType) {
    lastFilterUsed = filterType;

    var input, filter, table, tr, td, i;
    input = document.getElementById(filterType);
    filter = input.value.toUpperCase();
    table = document.getElementById("guestbookEntries");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        if (filterType === "userFilter")
            td = tr[i].getElementsByTagName("td")[0];
        else
            td = tr[i].getElementsByTagName("td")[1];

        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1)
                tr[i].style.display = "";
            else 
                tr[i].style.display = "none";
        }       
    }
}

/**
 * Filter with the last filter type used. Useful when refreshing messages.
 */
function searchFilterRefresh() {
    if (lastFilterUsed !== "") {
        searchFilter(lastFilterUsed);
    }
}