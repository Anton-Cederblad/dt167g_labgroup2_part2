/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Handles clicks on the button on the registeruser page.
 *
 */

let pwd1 = null;
let pwd2 = null;
let user = null;
let mail = null;
let button = null;

var recaptchaResponse = "";

function onInputChange() {

    // Enable button if the passwords matches and is not empty.
    button.disabled = !(pwd1.value === pwd2.value && pwd1.value !== "" && user.value !== "" && mail.value !== "");
}

// AJAX response handler
function responseHandler(response) {

    // Hide form
    document.getElementById('registerUserForm').classList.add('hidden');

    // Hide Password validation error message
    document.getElementById("errorMsg").style.display = "none";

    // Unhide the appropriate response.
    if (response === true)
        document.getElementById('success').classList.remove('hidden');
    else // Just fail if true wasn't received.
        document.getElementById('fail').classList.remove('hidden');
}

// Function that validates user creation password
function validateFormInput(){

    var errorMsg = "";
    var pwd1 = document.getElementById("pwd1");
    var pwd2 = document.getElementById("pwd2");
    var uname = document.getElementById("user");


    if(pwd1.value != "" && pwd1.value == pwd2.value || uname.value!= ""){

            if(uname.value.length > 50)
            {
              errorMsg = " Username is to long. ";
              document.getElementById( "errorMsg" ).innerHTML = errorMsg;
              return false;
            }

            if (pwd1.value.length < 8) {
                errorMsg = " Your password is too short. ";
                document.getElementById( "errorMsg" ).innerHTML = errorMsg;
                return false;
            }

            re = /[a-z]/;
            if (!re.test(pwd1.value)) {
                errorMsg = " You have to enter a lowercase letter. ";
                document.getElementById( "errorMsg" ).innerHTML = errorMsg;
                return false;
            }

            re = /[A-Z]/;
            if (!re.test(pwd1.value)) {
                errorMsg = " You have to enter a uppercase letter. ";
                document.getElementById( "errorMsg" ).innerHTML = errorMsg;
                return false;
            }

            re = /[0-9]/;
            if (!re.test(pwd1.value)) {
                errorMsg = " You have to enter a number. ";
                document.getElementById( "errorMsg" ).innerHTML = errorMsg;
                return false;
            }

            re = /[!@#$%^&*(),.?":{}|<>]/;
            if(!re.test(pwd1.value)){
                errorMsg = " You have to enter a special char. ";
                document.getElementById( "errorMsg" ).innerHTML = errorMsg;
                return false;
            }
            return true;
        }
    else{
        return false;
    }
}

function doRegisterUser() {

    if(validateFormInput()) {

        recaptchaResponse = grecaptcha.getResponse();

        if (pwd1.value === pwd2.value && pwd1.value !== "" && user.value !== "" && mail.value !== "" && recaptchaResponse.length > 0 ) {

            button.removeEventListener('click', doRegisterUser);

            ajax.post("reguser", [user.value, mail.value, pwd1.value, recaptchaResponse], responseHandler);
        }
    }
}

function init() {

    pwd1 = document.getElementById("pwd1");
    pwd2 = document.getElementById("pwd2");
    user = document.getElementById("user");
    mail = document.getElementById("email");
    button = document.getElementById("registerUserButton");

    pwd1.addEventListener('keyup', onInputChange);
    pwd2.addEventListener('keyup', onInputChange);
    user.addEventListener('keyup', onInputChange);
    mail.addEventListener('keyup', onInputChange);

    button.disabled = true;
    button.addEventListener('click', doRegisterUser);
}

window.addEventListener('load', init, false);
