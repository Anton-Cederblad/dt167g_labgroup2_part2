/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * JS for Reset Password page.
 * Validates that the two password inputs matches each other when typing
 * and sends a request for password reset on button press.
 */


let pwd1 = null;
let pwd2 = null;
let btn = null;

function onPwdInputChange() {

    // Enable button if the passwords matches and is not empty.
    btn.disabled = !(pwd1.value === pwd2.value && pwd1.value !== "");
}

// AJAX response handler
function responseHandler(response) {

    // Hide form
    document.getElementById('resetPwdForm').classList.add('hidden');

    // Unhide the appropriate response.
    if (response === true)
        document.getElementById('success').classList.remove('hidden');
    else if (response === false)
        document.getElementById('fail').classList.remove('hidden');
}

function onClick() {

    if (pwd1.value === pwd2.value && pwd1.value !== "") {

        btn.removeEventListener('click', onClick);

        // Gets everything after the "?" from the URL and splits it on "=".
        let getParams = window.location.search.substr(1).split("=");

        // The token is located at the end of the URL. (email=xxxx&token=yyyy)
        ajax.post('resetpwd', [getParams[getParams.length - 1], pwd1.value], responseHandler);
    }

}

function init() {
    pwd1 = document.getElementById("pwd1");
    pwd2 = document.getElementById("pwd2");
    btn = document.getElementById("setNewPwdButton");

    pwd1.addEventListener('keyup', onPwdInputChange);
    pwd2.addEventListener('keyup', onPwdInputChange);
    btn.disabled = true;
    btn.addEventListener('click', onClick);
}
window.addEventListener('load', init, false);