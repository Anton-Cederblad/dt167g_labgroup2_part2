/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Downloads and add messages to page DOM.
 *
 */

var messages = {};
(function () {
    this._postMsgFormId = "postMessage";
    this._containerId = "guestbookEntries";
    this._textInputId = "text";
    this._postButtonId = "submitMessage";
    this._container = null;


    // ========================================================================
    // Common
    // ========================================================================

    /**
     * Initializes messages module. Needs to be called on page load.
     */
    this.init = function() {
        messages._container = document.getElementById(messages._containerId);
        messages.download();

        // Post functionality
        var button = document.getElementById(messages._postButtonId);
        button.addEventListener("click", messages.onPostButton);

        var textInput = document.getElementById(messages._textInputId);
        textInput.addEventListener("keyup", messages.onMessageTextInput);

        // Only show post message form if a user is logged in
        var postForm = document.getElementById(messages._postMsgFormId);
        if (auth.isLoggedin()) {
            postForm.className = "visible";
        } else {
            postForm.className = "hidden";
        }
    };


    // ========================================================================
    // Download messages
    // ========================================================================

    /**
     * Clear messages currently on the page.
     * @private
     */
    this._clearDom = function() {
        // Assume "header" row is first. Remove all rows below.
        var tr = messages._container.getElementsByTagName("tr");
        while (tr.length > 1) {
            tr[1].parentNode.removeChild(tr[1]);
        }
    };

    /**
     * Adds DOM nodes to show a message.
     * @param msg {array} Message to show.
     * @private
     */
    this._renderMessage = function(msg) {
        var name    = document.createElement("td");
        var content = document.createElement("td");
        var time    = document.createElement("td");
        var votes   = document.createElement("td");
        var del = document.createElement("td");

        var deleteButton = document.createElement("button");
        deleteButton.innerText = "Delete";
        deleteButton.setAttribute("id","deleteButton_" + msg.id);
        deleteButton.setAttribute("name", msg.id);
        deleteButton.disabled = true;
        del.appendChild(deleteButton);

        // Enable delete button if the message owner is logged in.
        if (auth.getUser() === msg.username) {
            deleteButton.disabled = false;
        }

        name.innerText    = msg.username;
        content.innerText = msg.content;
        time.innerText    = msg.time;
        votes.innerText   = msg.vote;

        // Start - Vote column render

        var upVote = document.createElement("a");
        var plusSymbol = document.createTextNode("\u2191");
        upVote.appendChild(plusSymbol);
        upVote.setAttribute("id", "upvote_" + msg.id);
        upVote.setAttribute("name", msg.id);
        upVote.setAttribute("class", "voteSymbol");
        votes.appendChild(upVote);

        var downVote = document.createElement("a");
        var minusSymbol = document.createTextNode("\u2193");
        downVote.appendChild(minusSymbol);
        downVote.setAttribute("id", "downvote_" + msg.id);
        downVote.setAttribute("name", msg.id);
        downVote.setAttribute("class", "voteSymbol");
        votes.appendChild(downVote);

        var removeVote = document.createElement("a");
        var crossSymbol = document.createTextNode("x");
        removeVote.appendChild(crossSymbol);
        removeVote.setAttribute("id", "removevote_" + msg.id);
        removeVote.setAttribute("name", msg.id);
        removeVote.setAttribute("class", "voteSymbol");
        votes.appendChild(removeVote);

        // End - Vote column render

        var tr = document.createElement("tr");
        tr.appendChild(name);
        tr.appendChild(content);
        tr.appendChild(time);
        tr.appendChild(votes);
        tr.appendChild(del);

        messages._container.appendChild(tr);
        messages._enableDeleteButton(msg.id);
        messages._setUpvoteListener(msg.id);
        messages._setDownvoteListener(msg.id);
        messages._setRemovevoteListener(msg.id);
    };

    /**
     * Handles response of getmsg request.
     * @param response Array of messages.
     * @private
     */
    this._getmsgResponse = function(response) {
        if (response === null || response.length === 0) {
            console.log("No messages received from server. :(");
        }

        // Remove currently displayed messages so it's possible to download messages
        // multiple times.
        messages._clearDom();

        // Add messages to DOM
        for (var i = 0; i < response.length; i++) {
            var msg = response[i];
            messages._renderMessage(msg);
        }

        searchFilterRefresh();
    };

    /**
     * Downloads and displays all messages on the page. Removes previously
     * downloaded messages from the page.
     */
    this.download = function() {
        ajax.post("getmsg", null, messages._getmsgResponse);
    };


    // ========================================================================
    // Post a new message
    // ========================================================================

    this._enablePostButton = function(isEnabled) {
        var button = document.getElementById(messages._postButtonId);
        button.addEventListener("click", messages.onPostButton);
        button.disabled = !isEnabled;
    }

    /**
     * Sets the event handler of the delete button for each message
     * @param id
     */
    this._enableDeleteButton = function(id) {
        var deleteButton = document.getElementById("deleteButton_" + id);
        deleteButton.addEventListener("click",messages.onDeleteButton);
    }

    /**
     * Handles response for post message request. Reloads all messages when the
     * post message request was successful.
     * @param response
     * @private
     */
    this._postmsgResponse = function(response) {
        if (response === null || response === false) {
            console.log("Post message failed.");
        }

        // Make the new message visible
        messages.download();
    };

    /**
     * Handles response for delete message request. Reloads all messages when the
     * delete message request was successful.
     * @param response
     * @private
     */
    this._delmsgResponse = function(response){
        if (response === null || response === false) {
            console.log("Delete message failed.");
        }

        // Make the new message visible
        messages.download();
    }

    /**
     * Deletes the message for which the delete button was pressed and
     * change header color
     * @param e
     */
    this.onDeleteButton = function(e) {
        ajax.post("delmsg", parseInt(e.currentTarget.name), messages._delmsgResponse);
        var headers = document.getElementsByTagName("th");
        for(var i = 0; i < headers.length - 1; i++){
          if(headers[i].id === "time")
            headers[i].classList.add("headerActive");
          else
            headers[i].classList.remove("headerActive");

        }
    }

    /**
     * Posts a new message. Uses content of message input field as message
     * content.
     */
    this.onPostButton = function() {
        var textInput = document.getElementById(messages._textInputId);
        var text = textInput.value;
        var messageError="";
        textInput.value = "";

        messages._enablePostButton(false);

        if(text.length < 250){
          ajax.post("postmsg", text, messages._postmsgResponse);
        }
        else {
          messageError = "A post can only contain 250 characters";
          document.getElementById("messagerr").innerHTML = messageError;
        }
    };

    /**
     * Enables post button if there is text in message input field.
     */
    this.onMessageTextInput = function() {
        var textInput = document.getElementById(messages._textInputId);
        var text = textInput.value;

        messages._enablePostButton(text !== "");
    }

    // ========================================================================
    // Handle votes
    // ========================================================================

    /**
     * Registers upvote link listener
     */
    this._setUpvoteListener = function(id) {
        var upvote = document.getElementById("upvote_" + id);
        upvote.addEventListener("click", messages.onUpvote);
    }

    /**
     * Registers downvote link listener
     */
    this._setDownvoteListener = function(id) {
        var downvote = document.getElementById("downvote_" + id);
        downvote.addEventListener("click", messages.onDownvote);
    }

    /**
     * Registers removevote link listener
     */
    this._setRemovevoteListener = function(id) {
        var removevote = document.getElementById("removevote_" + id);
        removevote.addEventListener("click", messages.onRemovevote);
    }

    /**
     * Handles response for vote request. Reloads all messages when the
     * vote request was successful.
     * @param response
     * @private
     */
    this._voteResponse = function(response){
        if (response === null || response === false) {
            console.log("Vote failed.");
        }

        // Make the new message visible
        messages.download();
    }

    /**
     * Processes upvote. Data array includes message id and vote direction
     * @param e
     */
    this.onUpvote = function(e) {
        var paramArrayUp = [parseInt(e.currentTarget.name), "up"];
        ajax.post("vote", paramArrayUp, messages._voteResponse);
    }

    /**
     * Processes upvote. Data array includes message id and vote direction
     * @param e
     */
    this.onDownvote = function(e) {
        var paramArrayDown = [parseInt(e.currentTarget.name), "down"];
        ajax.post("vote", paramArrayDown, messages._voteResponse);
    }

    /**
     * Processes removevote. Data array includes message id and a null dummy
     * @param e
     */
    this.onRemovevote = function(e) {
        var paramArrayRemove = [parseInt(e.currentTarget.name), null];
        ajax.post("vote", paramArrayRemove, messages._voteResponse);
    }


}).apply(messages);

// messages.init will be called when login status is received from the server.
// see auth.js / auth.updatepage().
// Load messages on page load.
// window.addEventListener("load", messages.init, false);
