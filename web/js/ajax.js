/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Helper functions to make AJAX requests with XHR
 *
 */

var ajax = {};
(function () {
    /**
     * Requests are sent to this page.
     * @type {string}
     * @private
     */
    this._requestPage = "request.php";

    /**
     * Sets listener for readystatechange event of an XHR-object.
     * @param xhr {XMLHttpRequest}
     * @param responseHandler {function(response)} Called when response is received.
     * @private
     */
    this._setListener = function(xhr, responseHandler) {
        function listener() {
            // Ignore all state changes except DONE, nothing to done on the other states.
            if (xhr.readyState === XMLHttpRequest.DONE) {
                // 200 should be the only status code for a successful request.
                if (xhr.status === 200) {
                    var response = JSON.parse(xhr.response);
                    responseHandler(response);
                } else {
                    // Let caller know something went wrong.
                    responseHandler(null);
                }

                xhr.removeEventListener("readystatechange", listener, false)
            }
        }
        if (responseHandler !== null)
            xhr.addEventListener("readystatechange", listener, false);
    };

    /**
     * Sends a POST request with specified request name and parameters.
     * responseHandler is called when a response is received. The received data
     * is be passed as a parameter. null will be passed when request is
     * unsuccessful.
     *
     * Example:
     * function handler(response) {
     *     if (response !== null) {
     *         console.log("Data received OK!");
     *     } else {
     *         console.log("Something went wrong :(");
     *     }
     * }
     * ajax.post("getmsg", <optional parameter>, handler);
     *
     * @param requestName {string} Name of request, see RequestHandler::$classes in RequestHandler.class.php.
     * @param requestParams Anything that can be converted to JSON.
     * @param responseHandler {function(response)} Called when a response is received from server.
     */
    this.post = function(requestName, requestParams, responseHandler) {
        // This is how RequestHandler.class.php expect the data to look like.
        var data = {
            request: requestName,
            params:  requestParams
        };

        var xhr = new XMLHttpRequest();
        this._setListener(xhr, responseHandler);
        xhr.open("POST", this._requestPage, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(data));
    };

    /**
     * Same as post() except it's using GET as HTTP method. Sends requestParams
     * as URL parameters.
     * @param requestName
     * @param requestParams
     * @param responseHandler
     */
    this.get = function(requestName, requestParams, responseHandler) {
        // todo: Implement this if we want to use GET instead of POST for "get" requests.
    }
}).apply(ajax);

/*
// Test code:
function handler(response) {
    if (response !== null) {
            console.log(response);
        } else {
            console.log("Something went wrong :(");
        }
}
 ajax.post("vote", "hello", handler);
 */