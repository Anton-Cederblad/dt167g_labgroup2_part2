/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Handles authentication
 *
 */

var auth = {};
(function () {
    // User name of currently logged in user
    this._currentUser = "";

    this._authMsgId = "authMsg";
    this._userSpanId = "loggedInUser";

    // Elements related to the login
    this._loginDivId = "login";
    this._loginBtnId = "loginButton";
    this._userInputId = "uname";
    this._passInputId = "psw";

    // Elements related to the logout
    this._logoutDivId = "logout";
    this._logoutBtnId = "logoutButton";


    // ========================================================================
    // Common
    // ========================================================================

    /**
     * Initializes authentication module. Needs to be called on page load.
     */
    this.init = function() {
        // Button listeners
        var button;
        button = document.getElementById(auth._loginBtnId);
        button.addEventListener("click", auth.login);
        button = document.getElementById(auth._logoutBtnId);
        button.addEventListener("click", auth.logout);

        auth._updateLoginStatus();
    };

    /**
     * @returns {string} User name of currently logged in user. "" if no user is
     * logged in.
     */
    this.getUser = function() {
        return auth._currentUser;
    };

    /**
     * @returns {boolean} true if a user is logged in otherwise false.
     */
    this.isLoggedin = function() {
        return auth._currentUser !== "";
    };

    /**
     * Shows a message to inform the user about an error, for example wrong
     * password.
     * @param message {string} Message
     * @private
     */
    this._setMessage = function(message) {
        var e = document.getElementById(auth._authMsgId);
        e.innerText = message;
    };

    /**
     * Shows or hides login/logout form depending on if a user is logged in.
     * @private
     */
    this._updatePage = function() {
        var username = auth.getUser();

        // Page elements
        var loginDiv = document.getElementById(auth._loginDivId);
        var logoutDiv = document.getElementById(auth._logoutDivId);
        var userSpan = document.getElementById(auth._userSpanId);

        // Is a user logged in?
        if (username === null || username === "") {
            console.log("user logged out");
            // Show login form
            loginDiv.className = "visible";
            logoutDiv.className = "hidden";
            userSpan.innerText = "";
        } else {
            console.log("user " + username + " logged in");
            // Show logout form
            loginDiv.className = "hidden";
            logoutDiv.className = "visible";
            userSpan.innerText = username;
        }

        // Update message part of the page to reflect login status.
        // Todo: Make this less hacky. The message part of the page needs to
        // be updated to reflect login status.
        messages.init();

        // Clear current message
        auth._setMessage("");
    };

    // ========================================================================
    // Login
    // ========================================================================

    /**
     * Handles login response
     * @param response
     * @private
     */
    this._loginResponse = function(response) {
        if (response !== null && response !== "") {
            if(response == "Account is locked out. Please wait a few min before trying again."){
                auth._setMessage("Account is locked. Please wait a few min before trying again.")
            }else{
                auth._currentUser = response;
                auth._updatePage();   
            }
        } else {
            auth._setMessage("Wrong password or username!");
        }
    };

    /**
     * Make a login attempt by sending login form to the server.
     */
    this.login = function() {
        var userInput = document.getElementById(auth._userInputId);
        var passInput = document.getElementById(auth._passInputId);
        var user = userInput.value;
        var pass = passInput.value;

        if (user === "" || pass === "") {
            auth._setMessage("Enter username and password.");
            return;
        }

        userInput.value = "";
        passInput.value = "";

        var params = {
            command:"login",
            username:user,
            password:pass
        };

        ajax.post("auth", params, auth._loginResponse);
    };


    // ========================================================================
    // Logout
    // ========================================================================

    /**
     * Handles logout response.
     * @private
     */
    this._logoutResponse = function() {
        auth._currentUser = "";
        auth._updatePage();
    };

    /**
     * Logs out the current logged in user.
     */
    this.logout = function() {
        var params = {
            command:"logout"
        };

        ajax.post("auth", params, auth._logoutResponse);
    };


    // ========================================================================
    // Update page with login status
    // ========================================================================

    /**
     * Updates page to reflect login status.
     * @param response
     * @private
     */
    this._getUserResponse = function(response) {
        if (response !== null) {
            auth._currentUser = response;
        } else {
            auth._currentUser = "";
        }

        auth._updatePage();
    };

    /**
     * Gets login status from server.
     * @private
     */
    this._updateLoginStatus = function() {
        var params = {
            command:"getuser"
        };

        ajax.post("auth", params, auth._getUserResponse);
    }

}).apply(auth);

window.addEventListener("load", auth.init, false);
