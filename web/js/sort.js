/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Sort tables by clicking header.
 *
 */

// ========================================================================
// Simple sorting algorithm
// ========================================================================


function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("guestbookEntries");
  switching = true;
  dir = "asc";
  while (switching) {
    switching = false;
    rows = table.getElementsByTagName("TR");
    for (i = 1; i < (rows.length - 1); i++) {
      shouldSwitch = false;
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      switchcount ++;
    } else {
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}


window.addEventListener("load", sortTable.bind(null), false);

// Add color for selected header
function setColors(){
  var isColorSet = false;
  var headers = document.getElementsByTagName("th");

  for(var i = 0; i < headers.length - 1; i++){
    headers[i].addEventListener("click", function(event){
      for(var i = 0; i < headers.length -1 ; i++){
        if(event.target.id == headers[i].id){
          if(!headers[i].classList.contains("headerActive"))
            headers[i].classList.add("headerActive")
        }
        else
          headers[i].classList.remove("headerActive");
        }
    }, false);
  }
}

window.addEventListener("load", setColors, false);
