<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Forgot password page
 */

 //Uncomment the below section to add a new user with correctly hashed info (for testing purposes.)
 //require_once 'autoloader.php';
 //$db = Database::getInstance();
 //$db -> addUser("my_username", password_hash("my_password", PASSWORD_BCRYPT), password_hash("my_email_address", PASSWORD_BCRYPT));


// If user is logged in, they don't need access to this page.
if(isset($_SESSION['user'])){
    header("Location: index.php");
    die();
}

?>

<!DOCTYPE html>
<html lang="sv-SE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/ajax.js"></script>
    <script src="js/forgotpassword.js"></script>
    <title>Forgot password</title>
</head>

<body>
<header>
    <h1>Software Security Lab</h1>
</header>

<h2>Password reset</h2>

<form id="forgotPwdForm" action="" method="POST">
    <label><b>Username</b></label>
    <input type="text" placeholder="Enter username" name="uname" id="uname"
           required>
    <label><b>Email address</b></label>
    <input type="email" placeholder="Enter email address" name="email" id="email"
           required>
    <button type="button" id="forgotPwdButton">Reset password</button>
</form>
<p id="mailSent" class="hidden">We have sent you instructions on how to get a new password. <br>
    Please check your email account (it might be in your spam folder)
    <a href="index.php">Click here to go back</a></p>
</body>
</html>
