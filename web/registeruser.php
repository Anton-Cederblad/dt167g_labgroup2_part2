<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Register new user page
 */

// If user is logged in, they don't need access to this page.
if(isset($_SESSION['user'])){
    header("Location: index.php");
    die();
}

?>

<!DOCTYPE html>
<html lang="sv-SE">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css"/>
        <script src="js/ajax.js"></script>
        <script src="js/registeruser.js"></script>
        <title>Register new user</title>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>

    <body>
    <header>
        <h1>Software Security Lab</h1>
    </header>


    <h2>Register new user</h2>

    <p id="errorMsg"></p>

    <form id="registerUserForm" action="registeruser.php" method="POST">
        <label><b>Username</b></label><br>
        <input type="text" placeholder="Enter new username" name="user" id="user" required><br>
        <label><b>Email address</b></label><br>
        <input type="email" placeholder="Enter email address" name="email" id="email" required><br>
        <label><b>Password</b></label><br>
        <input type="password" placeholder="Enter new password" autocomplete="off" name="pwd1" id="pwd1" maxlength="100" required><br>
        <p>Password must contain minimum 8 chars both uppercase and lowercase letters, numbers and symbols</p>
        <label><b>Re-type password</b></label><br>
        <input type="password" placeholder="Confirm password" autocomplete="off" name="pwd2" id="pwd2" maxlength="100" required><br>
        <button type="button" id="registerUserButton">Register</button>
        <div class="g-recaptcha" data-sitekey="6LdbU10UAAAAAH_BHfMMX8rlTbmmT8WQonENpOab"></div>
    </form>

    <p id="success" class="hidden">Done! You can now login and post messages.<br>
        <a href="index.php">Click here to go back</a>
    </p>
    <p id="fail" class="hidden">Something went wrong, user was not created.<br>
        <a href="index.php">Click here to go back</a>
    </p>
    </body>
</html>
