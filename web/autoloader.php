<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Registers an autoloader to load classes in ./classes directory.
 */

/**
 * @param $class string Class name
 */
function autoloader($class) {
    include __DIR__ . '/classes/' . $class . '.class.php';
}

spl_autoload_register('autoloader');
