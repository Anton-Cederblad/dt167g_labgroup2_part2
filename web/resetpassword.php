<?php
/**
 * Lab Assignment (Group 1)
 * DT167G - Software Security
 *
 * Forgot password page
 */

require_once 'autoloader.php';

$validRequest = false;

if ( isset($_GET['token']) && isset($_GET['email']) ) {

    // Check if the token is valid and if the token belongs to the supplied email address.


    $db = Database::getInstance();

    $inputToken = $_GET['token'];
    $inputMail = $_GET['email'];

    // A real hash is needed to run password_verify against later, even if this is not a valid request, to prevent
    // timing attacks.
    $realMail = password_hash("This could not possibly be a valid mail address", PASSWORD_BCRYPT);

    $res = $db -> getMailHashFromToken(hash("sha256", $inputToken));

    if ($res)
        $realMail = $res;

    if ( password_verify(strtolower($inputMail), $realMail) ){
        $validRequest = true;
        $db->addLog("GOOD RESET TOKEN",$_SERVER['REMOTE_ADDR']);
    }else{
        $db->addLog("BAD RESET TOKEN",$_SERVER['REMOTE_ADDR']);
    }
}


if ( !$validRequest )
    header("Location: index.php");

?>

<!DOCTYPE html>
<html lang="sv-SE">
<?php if ($validRequest) : //Sanity check in case it is possible to circumvent the header redirect above.?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/ajax.js"></script>
    <script src="js/resetpassword.js"></script>
    <title>Set new password</title>
</head>

<body>
<header>
    <h1>Software Security Lab</h1>
</header>


<h2>Set new password</h2>

<form id="resetPwdForm" action="resetpassword.php" method="POST">
    <input type="password" placeholder="Enter new password" autocomplete="off" name="pwd1" id="pwd1"
           required>
    <input type="password" placeholder="Confirm password" autocomplete="off" name="pwd2" id="pwd2"
           required>
    <button type="button" id="setNewPwdButton">Change password</button>
</form>
<p id="success" class="hidden">Done! You can now use your new password to login.</p>
<p id="fail" class="hidden">Something went wrong, your password is NOT changed.</p>
</body>
<?php endif; ?>
</html>
