CREATE TABLE messageapp.log (
    id          SERIAL PRIMARY KEY,
    stamp        TIMESTAMP DEFAULT NOW(),
    event     text,
    value      text
)
WITHOUT OIDS;