-- Creates messageapp schema
-- Creates User table
-- Creates Post table
-- Creates Vote table

DROP SCHEMA IF EXISTS messageapp CASCADE;

CREATE SCHEMA messageapp;

DROP TABLE IF EXISTS messageapp.user;

CREATE TABLE messageapp.user (
    id          SERIAL PRIMARY KEY,
    username    text NOT NULL CHECK (username <> ''),
    pwdHash     text NOT NULL CHECK (pwdHash <> ''),
    mailHash    text,
    resetPwdToken text,
    resetPwdTime integer,
    CONSTRAINT unique_username UNIQUE(username)
)
WITHOUT OIDS;


DROP TABLE IF EXISTS messageapp.post;

CREATE TABLE messageapp.post (
    id          SERIAL PRIMARY KEY,
    time        TIMESTAMP DEFAULT NOW(),
    content     text NOT NULL CHECK (content <> ''),
    userId      integer REFERENCES messageapp.user(id) NOT NULL
)
WITHOUT OIDS;


DROP TABLE IF EXISTS messageapp.vote;

CREATE TABLE messageapp.vote (
    userId      integer REFERENCES messageapp.user(id),
    postId      integer REFERENCES messageapp.post(id),
    vote        integer CHECK (vote = 1 OR vote = -1),
	PRIMARY KEY(userId, postId)
)
WITHOUT OIDS;
