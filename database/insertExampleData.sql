-- The easiest way to test the mailing capability is to 
-- use the commented section in the php-header on forgotpassword.php to create a new user

-- password="password1" mail="alfa@test.com"
INSERT INTO messageapp.user(username, pwdHash, mailHash) VALUES('alfa', '$2y$10$wedW61eh7flhuLTLugDALe0jlv9d5MQ/5fxDiBbr8co.Dj3Kh2BNW', '$2y$10$sys6/6ZzzpERMHl.xTf45egj2drASQnpn3f5zBrpV/HjgxpD0gf4m');

-- password="password2" mail="beta@test.com"
INSERT INTO messageapp.user(username, pwdHash, mailHash) VALUES('beta', '$2y$10$9G3nfj82G.1GqoLzR1hOkOX7/Bt13HttGqh9.7gdcXUtVAwak4euu', '$2y$10$w1GyFgyszC49PKezNRS/yO26B/k..Kj8ZqT2Q8W8ChkO6CvoiVF4.');

-- password="password3" mail="gamma@test.com"
INSERT INTO messageapp.user(username, pwdHash, mailHash) VALUES('gamma', '$2y$10$tYPUSZuv0neFn6eZzc.v/O/U6fzBW/Y1B886wWHY5qP4.fVTB7i7G', '$2y$10$bqcxCeIMxy1xguvvuItynORXeEbwzAL56wiKdf5vyS/sDr.FtfZ6e');


INSERT INTO messageapp.post(time, content, userId) VALUES('2018-02-01 12:00:00', 'Lorem ipsum dolor sit amet,', 1);

INSERT INTO messageapp.post(time, content, userId) VALUES('2018-02-01 13:22:23','consectetur adipiscing elit. Pellentesque et ',2);

INSERT INTO messageapp.post(time, content, userId) VALUES('2018-02-03 09:10:22','libero lorem. Suspendisse vitae nibh. ',3);



INSERT INTO messageapp.vote(userId, postId, vote) VALUES(1, 1, 1);
INSERT INTO messageapp.vote(userId, postId, vote) VALUES(2, 1, 1);
INSERT INTO messageapp.vote(userId, postId, vote) VALUES(3, 1, -1);
INSERT INTO messageapp.vote(userId, postId, vote) VALUES(1, 2, -1);
INSERT INTO messageapp.vote(userId, postId, vote) VALUES(2, 2, -1);
INSERT INTO messageapp.vote(userId, postId, vote) VALUES(3, 2, -1);
INSERT INTO messageapp.vote(userId, postId, vote) VALUES(1, 3, 1);
INSERT INTO messageapp.vote(userId, postId, vote) VALUES(2, 3, 1);
INSERT INTO messageapp.vote(userId, postId, vote) VALUES(3, 3, 1);
