-- Register an upvote from an user to a post (downvote is the same but vote=-1 instead of 1. )
INSERT INTO messageapp.vote(userId, postId, vote) VALUES($votingUser, $votedPost, 1) 
    ON CONFLICT(userId, postId) 
    DO UPDATE SET vote = EXCLUDED.vote;


-- Register a post from a user (time could be omitted, the database would then use the default NOW() value )
INSERT INTO messageapp.post(time, content, userId) VALUES ('2001-01-01 12:02:03', 'post content', $postingUser);


-- Delete a post
DELETE FROM messageapp.post WHERE id = $postId;


-- Get username, timestamp and content for all posts.
SELECT username, time, content 
FROM messageapp.post LEFT JOIN messageapp.user ON messageapp.user.id = userId;


-- Get votes for specific post (Coalesce is used to return 0 when/if there are no registered votes)
SELECT COALESCE(SUM(vote), 0) AS vote 
FROM messageapp.vote 
WHERE postId = 1;


-- Or get info on posts and votes in one go
SELECT username, time, content, COALESCE(vote, 0) AS vote 
FROM (messageapp.post LEFT JOIN 
    (SELECT postId, SUM(vote) AS vote FROM messageapp.vote GROUP BY postId) AS qry1 
    ON id = postId)  LEFT JOIN 
    messageapp.user ON messageapp.user.id = userId;


-- Filter posts on keyword (might be done in JavaScript instead of repeatedly querying the database?)
SELECT username, time, content 
FROM messageapp.post LEFT JOIN messageapp.user ON messageapp.user.id = userId
WHERE LOWER(content) LIKE LOWER('%keyword%');


